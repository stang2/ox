/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.STANG;

/**
 *
 * @author THANAWAT_TH
 */
import java.util.Scanner;

public class OX {

    public static void showarr(String[][] arr) {
        System.out.println("  1 2 3");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(i + 1 + " ");
            for (int k = 0; k < arr.length; k++) {
                System.out.print(arr[i][k] + " ");
            }
            System.out.println("");
        }

    }

    public static void main(String[] args) {
        String arr[][] = new String[3][3];
        Scanner s = new Scanner(System.in);
        System.out.println("Welcome to OX Game");
        for (int i = 0; i < arr.length; i++) {
            for (int k = 0; k < arr.length; k++) {
                arr[i][k] = "-";
            }

        }
        showarr(arr);

        int countx = 0;
        int counto = 0;
        String win = "draw";
        while (win.equals("draw")) {
            System.out.println("Turn X");
            System.out.println("Please input Row Col: ");
            int rowx = s.nextInt();
            int colx = s.nextInt();
            arr[rowx - 1][colx - 1] = "X";
            showarr(arr);
            if ((arr[0][0].equals("X") && arr[0][1].equals("X") && arr[0][2].equals("X")) || (arr[1][0].equals("X") && arr[1][1].equals("X") && arr[1][2].equals("X")) || (arr[2][0].equals("X") && arr[2][1].equals("X") && arr[2][2].equals("X")) || (arr[0][0].equals("X") && arr[1][0].equals("X") && arr[2][0].equals("X")) || (arr[0][1].equals("X") && arr[1][1].equals("X") && arr[2][1].equals("X")) || (arr[0][2].equals("X") && arr[1][2].equals("X") && arr[2][2].equals("X")) || (arr[0][0].equals("X") && arr[1][1].equals("X") && arr[2][2].equals("X")) || (arr[0][2].equals("X") && arr[1][1].equals("X") && arr[2][0].equals("X"))) {
                win = "win";
                System.out.println("X win");
                break;
            }
            countx++;
            if (countx == 5 && counto == 4) {
                System.out.println("This game is draw.");
                break;
            }
            System.out.println("Turn O");
            System.out.println("Please input Row Col: ");
            int rowo = s.nextInt();
            int colo = s.nextInt();
            arr[rowo - 1][colo - 1] = "O";
            showarr(arr);
            if ((arr[0][0].equals("O") && arr[0][1].equals("O") && arr[0][2].equals("O")) || (arr[1][0].equals("O") && arr[1][1].equals("O") && arr[1][2].equals("O")) || (arr[2][0].equals("O") && arr[2][1].equals("O") && arr[2][2].equals("O")) || (arr[0][0].equals("O") && arr[1][0].equals("O") && arr[2][0].equals("O")) || (arr[0][1].equals("O") && arr[1][1].equals("O") && arr[2][1].equals("O")) || (arr[0][2].equals("O") && arr[1][2].equals("O") && arr[2][2].equals("O")) || (arr[0][0].equals("O") && arr[1][1].equals("O") && arr[2][2].equals("O")) || (arr[0][2].equals("O") && arr[1][1].equals("O") && arr[2][0].equals("O"))) {
                win = "win";
                System.out.println("O win");
                break;
            }
            counto++;

        }
        System.out.println("Bye Bye . . .");
    }

}


